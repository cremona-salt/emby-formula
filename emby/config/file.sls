# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import emby with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

{%- set mountpoint = emby.docker.mountpoint %}

{%- for dir in [
  'docker/emby/config',
  'shares/media/tv',
  'shares/media/movies',
  'shares/audio/audiobooks',
  'shares/audio/music'
  ]
%}
emby-config-file-{{ dir|replace('/', '_') }}-directory-managed:
  file.directory:
    - name: {{ mountpoint }}/{{ dir }}
    - user: stooj
    - group: users
    - dir_mode: 0755
    - file_mode: 0644
    - makedirs: True
    - recurse:
      - mode
      - user
      - group
{% endfor %}

emby-config-file-emby-max-user-watches-configured:
  sysctl.present:
    - name: fs.inotify.max_user_watches
    - value: {{ emby.max_user_watches }}
