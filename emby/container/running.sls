# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import emby with context %}

{%- set mountpoint = emby.docker.mountpoint %}

emby-container-running-container-present:
  docker_image.present:
    - name: {{ emby.container.image }}
    - tag: {{ emby.container.tag }}
    - force: True

emby-container-running-container-managed:
  docker_container.running:
    - name: {{ emby.container.name }}
    - image: {{ emby.container.image }}:{{ emby.container.tag }}
    - restart: always
    - binds:
      - {{ mountpoint }}/docker/emby/config:/config
      - {{ mountpoint }}/shares/media/tv:/data/tvshows
      - {{ mountpoint }}/shares/media/movies:/data/movies
      - {{ mountpoint }}/shares/audio/audiobooks:/data/audiobooks
      - {{ mountpoint }}/shares/audio/music:/data/music
    - port_bindings:
      - 8196:8096
    - environment:
      - PUID: 1000
      - PGID: 100
      - TZ: Europe/London
    - labels:
      - "traefik.enable=true"
      - "traefik.http.middlewares.emby-redirect-websecure.redirectscheme.scheme=https"
      - "traefik.http.routers.emby-web.middlewares=emby-redirect-websecure"
      - "traefik.http.routers.emby-web.rule=Host(`{{ emby.container.url }}`)"
      - "traefik.http.routers.emby-web.entrypoints=web"
      - "traefik.http.routers.emby-websecure.rule=Host(`{{ emby.container.url }}`)"
      - "traefik.http.routers.emby-websecure.tls.certresolver=letsencrypt"
      - "traefik.http.routers.emby-websecure.tls=true"
      - "traefik.http.routers.emby-websecure.entrypoints=websecure"
